'''
	>> python getscores.py example.bitmap model
	model is the zinnia model file, obtained by running zinnia_learn
	on the training file
'''

import subprocess
import shlex
from svg.path import parse_path
import sys
import zinnia

def trace(inputfile):
	cmd = 'autotrace -output-format svg -output-file output.svg ' + inputfile
	cmd = shlex.split(cmd)
	subprocess.check_call(cmd)

	# remove input file
	#subprocess.check_call(['rm','-rf',inputfile])

	# parse image
	f = open('output.svg','r')
	paths = []
	x = []
	y = []
	for line in f:
		if line[0:5] == "<path":
			for i in range(len(line)):
				if line[i] == '#':
					a = i
					break
			if line[a:a+7].lower() == '#ffffff':
				isBackground = True
			else:
				isBackground = False
			if isBackground:
				# get canvas size
				start = []
				end = []
				for j in range(len(line)):
					if line[j].upper() == 'M':
						start.append(j)
					if line[j:j+2] == '/>':
						end.append(j-1)
				for k in range(len(start)):
					if k<=len(start)-2:
						p = parse_path(line[start[k]:start[k+1]]+'z')
						for m in range(len(p)):
							x.append(p[m].point(0.0).real)
							y.append(p[m].point(0.0).imag)
					else:
						p = parse_path(line[start[k]:end[0]])
						for m in range(len(p)):
							x.append(p[m].point(0.0).real)
							y.append(p[m].point(0.0).imag)
			else:
				# get paths
				start = []
				end = []
				for j in range(len(line)):
					if line[j].upper() == 'M':
						start.append(j)
					if line[j:j+2] == '/>':
						end.append(j-1)
				for k in range(len(start)):
					if k<=len(start)-2:
						paths.append(line[start[k]:start[k+1]]+'z')
					else:
						paths.append(line[start[k]:end[0]])
	w = int(max(x))
	h = int(max(y))
	f.close()

	# remove output file
	subprocess.check_call(['rm','-rf','output.svg'])

	# parse paths
	curves = []
	spacing = (w+h)/2.0/10.0 # spacing between points
	for k in paths:
		points = []
		p = parse_path(k)
		l = p.length()
		n = round(l/spacing) # number of points per curve
		if n <= 1:
			n = 2
		for i in range(int(n)):
			point = p.point(0.0+(i/(n-1)))
			x = point.real
			y = point.imag
			points.append([x,y])
		curves.append(points)
	
	# curves[path][point][coordinate]
	# curves[0][2][1] is the y coordinate of the 3rd point on the first svg path

	# S-expression
	strokes = ''
	for i in curves:
		stroke = ''
		for j in i:
			x = j[0]
			y = j[1]
			point = '(' + str(x) + ' ' + str(y) + ')'
			stroke += point
		strokes += '(' + stroke + ')'
	s = '(character (width ' + str(w) + ')(height ' + str(h) + ')(strokes ' + strokes + '))'

	return s

def get_scores(model, character):
	s = zinnia.Character()
	r = zinnia.Recognizer()
	r.open(model)

	if (not s.parse(character)):
		print s.what()
	result = r.classify(s, 10)
	size = result.size()
	for i in range(0, (size - 1)):
        	print "%s\t%f" % (result.value(i), result.score(i))
	return result 	


def main():
	i = sys.argv[1]
	model = sys.argv[2]
	s = trace(i)
	get_scores(model, s)


if __name__ == "__main__":
	main()


