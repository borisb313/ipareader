p bilabial stop voiceless
b bilabial stop voiced
m bilabial nasal voiced
ʍ bilabial glide voiceless
w bilabial glide voiced
f labiodental fricative voiceless
v labiodental fricative voiced
θ interdental fricative voiceless
ð interdental fricative voiced
t alveolar stop voiceless
d alveolar stop voiced
s alveolar fricative voiceless
z alveolar fricative voiced
n alveolar nasal voiced
l alveolar lateral approximant voiced
ɹ alveolar retroflex approximant
ʃ alveopalatal fricative voiceless
ʒ alveopalatal fricative voiced
ʧ alveopalatal affricate voiceless
ʤ alveopalatal affricate voiced
j palatal glide voiced
k velar stop voiceless
g velar stop voiced
ŋ velar nasal voiced
ʔ glottal stop voiceless
h glottal fricative voiceless
