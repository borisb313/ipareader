package edu.utexas.ipareader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class CharacterInfoActivity extends Activity {

	private final static String TAG = "InfoActivity";
	BufferedReader consReader;
	BufferedReader vowelReader;
	
	String charInfo;
	
	Character character;
	
	boolean isVowel = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		
		Intent intent = getIntent();
		String charValue = intent.getStringExtra("character");
		
		//read IPA info file
		try {
			AssetManager am = CharacterInfoActivity.this.getAssets();
			InputStream cons = am.open("ipa_american_english_consonants.txt");
			InputStreamReader consreader = new InputStreamReader(cons);
			consReader = new BufferedReader(consreader);
			InputStream vowels = am.open("ipa_american_english_vowels.txt");
			InputStreamReader vowelreader = new InputStreamReader(vowels);
			vowelReader = new BufferedReader(vowelreader);
		} catch (IOException e) {
			Log.i(TAG, "error opening text file");
			e.printStackTrace();
		}
		
		//get info about selected character
		try {
			boolean temp = true;
			while(temp){
				String info = consReader.readLine();
				if (info == null){
					break;
				}
				if (charValue.equals(info.substring(0, 1))) {
//					Log.i(TAG, "got the char! "+ info);
					charInfo = info;
					temp = false;
					isVowel = false;
				}
			}

			if (isVowel){
				while(temp){
					String info = vowelReader.readLine();

					if (charValue.equals(info.substring(0, 1))) {
						//					Log.i(TAG, "got the char! "+ info);
						charInfo = info;
						temp = false;
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//do something with the character
		String[] phoneticInfo = charInfo.split(" ");
		
		TextView value = (TextView)findViewById(R.id.char_value);
		TextView place = (TextView)findViewById(R.id.char_place);
		TextView manner = (TextView)findViewById(R.id.char_manner);
		TextView voicing = (TextView)findViewById(R.id.char_voicing);
		
		Typeface junicode = Typeface.createFromAsset(getAssets(), "fonts/Junicode.ttf");
		value.setTypeface(junicode);
		place.setTypeface(junicode);
		manner.setTypeface(junicode);
		voicing.setTypeface(junicode);
		
		if (!isVowel) {
			value.setText("Consonant: " +phoneticInfo[0]);
			place.setText("Place of articulation: "+phoneticInfo[1]);
			manner.setText("Manner of articulation: " + phoneticInfo[2]);
			voicing.setText("Voicing: "+phoneticInfo[3]);
		} else {
			value.setText("Vowel: " +phoneticInfo[0]);
			place.setText("Height: "+phoneticInfo[1]);
			manner.setText("Backness: " + phoneticInfo[2]);
			voicing.setText("Rounding: "+phoneticInfo[3]);
		}
		
		
	}
	
	
}
