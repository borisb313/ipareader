package edu.utexas.ipareader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class IPANetworking {

	private static final String url = 
			"http://ipaloadbalancer-1645884042.us-west-2.elb.amazonaws.com/upload_expression";
	private static final String TAG = "IPANetworking";
	private String image;
	
	public IPANetworking(String image) {
		this.image=image;
	}
	
	public JSONObject sendRequest(){
		
		InputStream inputStream = null;
	    String result = null;
		 
		List<NameValuePair> payload = new ArrayList<NameValuePair>();
		payload.add(new BasicNameValuePair("character", image));
		
		final DefaultHttpClient httpClient = new DefaultHttpClient();
        
        try {
        	//create the http request
            HttpPost postRequest = new HttpPost(url);
            postRequest.setHeader("Content-type",
                    "application/x-www-form-urlencoded");
            if (payload!=null) {
                postRequest.setEntity(new UrlEncodedFormEntity(payload));
            }
            
            HttpUriRequest request = postRequest;
            
            //get a response
            HttpResponse response = httpClient.execute(request);
            
            //do something with response
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();
            try {

                JSONObject json = new JSONObject(result);
                return json;

            } catch (JSONException ex) {
                throw new Exception("Could not parse JSON");
            }
            
        } catch (Exception e) {
//			e.printStackTrace();
        	Log.e(TAG, "Problem with response from server: " + e.toString());
			
		} finally {
			try {
	              if (inputStream != null)
	                  inputStream.close();
	          } catch (Exception ex) {
	              Log.e(TAG, "Something wrong with the input stream");
	          }
        }
        return null;
	}
	
}
