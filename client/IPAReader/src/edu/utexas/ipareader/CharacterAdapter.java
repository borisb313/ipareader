package edu.utexas.ipareader;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CharacterAdapter extends ArrayAdapter<Character> {

	ArrayList<Character> chars;
//	Typeface typeface;
	
	public CharacterAdapter(Context context, ArrayList<Character> chars) {
		super(context, 0, chars);
//		typeface = Typeface.createFromAsset(context.getAssets(), "Junicode.ttf");
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // Get the data item for this position
       Character user = getItem(position);    
       // Check if an existing view is being reused, otherwise inflate the view
       if (convertView == null) {
          convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_character, parent, false);
       }
       // Lookup view for data population
       TextView character = (TextView) convertView.findViewById(R.id.character);
       // Populate the data into the template view using the data object
       character.setText(user.getValue());
       // Return the completed view to render on screen
       return convertView;
   }
}
