package edu.utexas.ipareader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View {

	private static final String TAG = "DrawView";
	
	//drawing path
	private Path drawPath;
	//drawing and canvas paint
	private Paint drawPaint, canvasPaint;
	//canvas
	private Canvas drawCanvas;
	//canvas bitmap
	private Bitmap canvasBitmap;
	//are we erasing?
	private boolean erase=false;
	//make a character string
	StringBuilder builder;
	int drawingDimensions;
	StringBuilder temp;
	//bool
	boolean touched = false;
	
	
	public DrawView(Context context, AttributeSet attrs){
		super(context, attrs);
		setupDrawing();
	}

	public void startNew(){
	    drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
	    invalidate();
	    builder = new StringBuilder();
	    builder.append("(character (strokes ");
	}

	private void setupDrawing() {
		/* Setup the drawing stuff */

		drawPath = new Path();
		drawPaint = new Paint();
		drawPaint.setColor(0xFF000000);
		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(20);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);

		canvasPaint = new Paint(Paint.DITHER_FLAG);
		builder = new StringBuilder();
		builder.append("(character (strokes ");
	}

	@Override
	protected void onSizeChanged(int width, int height, int oldwidth, int oldheight) {
		//called when view gets assigned a size
		super.onSizeChanged(width, height, oldwidth, oldheight);
		
		//set the canvas to be a square
		canvasBitmap = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
		drawingDimensions = width;
		
	}

	@Override
	protected void onDraw(Canvas canvas) {
		//draw view
		canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
		canvas.drawPath(drawPath, drawPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//detect user touch   
		float touchX = event.getX();
		float touchY = event.getY();
//		Log.i(TAG, "x: "+touchX+" y: "+touchY);
		if (!touched) {
			temp = new StringBuilder();
			temp.append("(");
		}
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touched = true;
			drawPath.moveTo(touchX, touchY);
			temp.append("("+touchX+" "+touchY+")");
			break;
		case MotionEvent.ACTION_MOVE:
			drawPath.lineTo(touchX, touchY);
			temp.append("("+touchX+" "+touchY+")");
			break;
		case MotionEvent.ACTION_UP:
			touched = false;
			temp.append(")");
			builder.append(temp.toString());
			drawCanvas.drawPath(drawPath, drawPaint);
			drawPath.reset();
			break;
		default:
			return false;
		}

		//invalidate causes onDraw to execute	
		invalidate();
		return true;
	}
	
	public String getSExp(){
		builder.insert(11 , "(width "+drawingDimensions+")(height "
				+drawingDimensions+")");
		builder.append("))");
		return builder.toString();
	}

}
