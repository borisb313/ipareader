package edu.utexas.ipareader;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class MainScreenActivity extends Activity {

	private Button drawButton;
	private Button historyButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);

		//set title
		TextView title = (TextView)findViewById(R.id.title);
		TextView description = (TextView)findViewById(R.id.description);
		Typeface junicodeBold = Typeface.createFromAsset(getAssets(), "fonts/Junicode-Bold.ttf");
		Typeface junicode = Typeface.createFromAsset(getAssets(), "fonts/Junicode.ttf");
		title.setTypeface(junicodeBold);
		description.setTypeface(junicode);
		
		//set buttons
		drawButton = (Button) findViewById(R.id.button1);
		drawButton.setTypeface(junicode);
		drawButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent startDrawing =  new Intent(MainScreenActivity.this, DrawCharacterActivity.class);
				startActivity(startDrawing);
			}
		});
		historyButton = (Button) findViewById(R.id.button2);
		historyButton.setTypeface(junicode);
		historyButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
//				Intent startDrawing =  new Intent(MainScreenActivity.this, DrawCharacterActivity.class);
//				startActivity(startDrawing);
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
