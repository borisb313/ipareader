package edu.utexas.ipareader;

public class Character {
	private String value;
	private double score;
	
	public Character(String value, double score){
		this.value = value;
		this.score = score;
	}

	public String getValue() {
		return value;
	}

	public double getScore() {
		return score;
	}
}