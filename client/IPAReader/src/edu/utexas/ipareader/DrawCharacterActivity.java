package edu.utexas.ipareader;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DrawCharacterActivity extends Activity implements OnClickListener{

	//our custom draw view
	private DrawView drawView;
	//buttons
	private Button newBtn, sendBtn;
	//image file, made when user hits send button
	String encodedImage;
	//list of characters and scores
	ArrayList<Character> chars;
	CharacterAdapter charAdapter;
	// log tag
	private static final String TAG = "DrawCharacterActivity";
	// image
	String sExp;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_character);
        
        chars = new ArrayList<Character>();
        
        //set up the draw view, make it a square
        drawView = (DrawView)findViewById(R.id.drawing);
        drawView.post(new Runnable() {

            @Override
            public void run() {
                ViewGroup.LayoutParams params;
                params = drawView.getLayoutParams();
//                Log.i("DrawCharacterActivity", "height: "+drawView.getHeight()+ 
//                		" width: "+drawView.getWidth());
                params.height = drawView.getWidth();
                drawView.setLayoutParams(params);
                drawView.postInvalidate();
            }
        });
        
        //get the buttons
        newBtn = (Button)findViewById(R.id.new_btn);
        newBtn.setOnClickListener(this);
        sendBtn = (Button)findViewById(R.id.send_btn);
        sendBtn.setOnClickListener(this);
        
        //set fonts
        TextView instructions = (TextView)findViewById(R.id.instructions);
        Typeface junicodeBold = Typeface.createFromAsset(getAssets(), "fonts/Junicode-Bold.ttf");
		Typeface junicode = Typeface.createFromAsset(getAssets(), "fonts/Junicode.ttf");
		instructions.setTypeface(junicodeBold);
		newBtn.setTypeface(junicodeBold);
		sendBtn.setTypeface(junicodeBold);
		
    }

	@Override
	public void onClick(View view) {
		if(view.getId()==R.id.new_btn){
		    //new button clicked
			drawView.startNew();
		} else if(view.getId()==R.id.send_btn){
            //save and send drawing to server
			chars.clear();
			sExp = drawView.getSExp();
			Log.i(TAG, sExp);
			drawView.startNew();
			
			// get the characters
			sendFile();
		}
	}
	
	private void sendFile(){
		drawView.setDrawingCacheEnabled(true);
		
		// commented out code used encode draw view to an image
//		Bitmap bitmap = drawView.getDrawingCache();
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); //bitmap is the bitmap object   
//		byte[] bytearray = baos.toByteArray();
//		bytes = baos.toByteArray();
//		encodedImage = Base64.encodeToString(bytearray, Base64.DEFAULT);
		
		//get data with s expression
		new SendImageTask().execute();
		drawView.destroyDrawingCache();
	}
	
    private class SendImageTask extends AsyncTask<Void, Integer, String> {
        @Override
        protected String doInBackground(Void... p) {
            
        	//user IPAnetworking to make a request, get back results
            try {
            	IPANetworking net = new IPANetworking(sExp);
            	JSONObject jObj = net.sendRequest();
            	
            	if (jObj.getString("status").equals("success")){
            		JSONArray results = jObj.getJSONArray("result");
            		for (int i = 0; i < results.length(); i++) { 
            		     JSONObject j = results.getJSONObject(i);
            		     String value = j.getString("value");
            		     double score = j.getDouble("score");
            		     putChar(new Character(value, score));
//            		     Log.i(TAG, "value: "+value+" score: "+score);
            		}
            		return "woohoo!";
            	}
            	
            } catch (Exception e) {
                return "error: " + e;
            }
            return "something went wrong, check for daleks";
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        	
        	if (result.equals("woohoo!")){
//        		Toast toast = Toast.makeText(getApplicationContext(), 
//        		        "got the values!!!", Toast.LENGTH_SHORT);
//        		toast.show();
        		defineAdapter();
        		
        	} else {
        		Toast toast = Toast.makeText(getApplicationContext(), 
        		        "Couldn't get image details", Toast.LENGTH_SHORT);
        		toast.show();
        	}
       }
    }
    
    private void putChar(Character character) {
    	chars.add(character);
    }
    
    private void defineAdapter(){
    	//put characters in a list view
		charAdapter = new CharacterAdapter(this, chars);
		ListView listView = (ListView) findViewById(R.id.char_list);
		listView.setAdapter(charAdapter);
		listView.setOnItemClickListener(new OnItemClickListener()
		{
		    @Override 
		    public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
		    { 
		        Character c = (Character) charAdapter.getItem(position);
		        Intent intent = new Intent(DrawCharacterActivity.this, CharacterInfoActivity.class);
		        intent.putExtra("character", c.getValue());
                startActivity(intent);
		    }
		});
    }

	@Override
	protected void onRestart() {
		super.onRestart();
	}
	
}
