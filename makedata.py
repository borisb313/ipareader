from getscores import trace
import glob

def makedata():
	for f in glob.glob('*.bitmap'):
     		val = '(value {})'.format(f[0])
		char = trace(f)
		s = '{} {}{}'.format(char[0:10], val, char[10:])
		print s

if __name__ == '__main__':
	makedata()
